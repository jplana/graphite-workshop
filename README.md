Codemotion 2015 Graphite Workshop
=====================

This is the repository containing the infrastructure for the Graphite Workshop presented @ Codemotion 2015

----------


Bootstrapping the infrastructure
---------

### Pre-requirements

In order to follow the workshop you'll need the following stuff:

 - **A Laptop** with a recent version of Linux or Mac OSX (Windows should work too, but I have not tested anything and you'll be on your own though)
 - **Docker**
 - **Docker-Machine** _Even if you're using Linux_
   - **Virtualbox**
 - **Docker-compose**
 - **Git**

> **Tip:** See [Installing Docker Machine on Mac OS X & Windows](https://docs.docker.com/machine/install-machine/#os-x-and-windows)  for a complete guide on installing the required Docker stack.

### Preparation

If you want to avoid problems with the network it's recommended that you pre-download all the required materials:

- Clone this repository:
`git clone https://bitbucket.org/jplana/graphite-workshop.git`
- Create the Docker Machine if not already available:
```shell
$ docker-machine create --driver virtualbox default
Creating VirtualBox VM...
Creating SSH key...
Starting VirtualBox VM...
Starting VM...
To see how to connect Docker to this machine, run: docker-machine env default
$ eval "$(docker-machine env default)"
$ docker-machine active
default
```

> **Tip:** When working with Docker Machine, ensure that the environment in your shell is defined: `eval "$(docker-machine env default)"`


- Build the docker image: This will download and install all the required docker images.
```shell
$ eval "$(docker-machine env default)"
$ docker-compose pull
Pulling graphite (jplana/grafana-graphite-workshop-docker:latest)...
latest: Pulling from jplana/grafana-graphite-workshop-docker
5679b9b90e09: Pull complete
ea6bab360f56: Pull complete
97e4edf3a19b: Pull complete
0fb39c100670: Pull complete
4322def076bf: Pull complete
.
.
.

```
- Test everything was built correctly.
```shell
$ eval "$(docker-machine env default)"
$ docker-compose up
Starting graphiteworkshop_graphite_1...
Starting graphiteworkshop_ipython-notebook_1...
Starting graphiteworkshop_grafana_1...
Starting graphiteworkshop_nginx_1...
.
.
.
(Wait for it to finish then ^C)

$ docker-compose start
Starting graphiteworkshop_graphite_1...
Starting graphiteworkshop_ipython-notebook_1...
Starting graphiteworkshop_grafana_1...
Starting graphiteworkshop_nginx_1...

$ docker-compose ps
             Name                           Command                           State                            Ports
---------------------------------------------------------------------------------------------------------------------------------
graphiteworkshop_grafana_1       /bin/sh -c /usr/sbin/grafa ...   Up                               0.0.0.0:3000->3000/tcp
graphiteworkshop_graphite_1      /bin/sh -c supervisord --n ...   Up                               0.0.0.0:2003->2003/tcp,
                                                                                                   0.0.0.0:2004->2004/tcp,
                                                                                                   0.0.0.0:8000->8000/tcp
graphiteworkshop_ipython-        tini -- start-notebook.sh  ...   Up                               0.0.0.0:8888->8888/tcp
notebook_1
graphiteworkshop_nginx_1         nginx -g daemon off;             Up                               0.0.0.0:443->443/tcp,
                                                                                                   0.0.0.0:80->80/tcp
```

- Get your docker-machine IP

```shell
$ eval "$(docker-machine env default)"
$ docker-machine ip default
192.168.99.100
```


> **Tip:** You will need this IP to access all the services, and this IP *may* change if the docker-machine is restarted

- Configure grafana

```shell
$ eval "$(docker-machine env default)"
$ pwd
/Users/XXX/src/graphite-workshop
$ cd scripts/grafana
$./configure.sh $(docker-machine ip default)
```

Available services
------------------

Assuming that your docker-machine ip is *192.168.99.100* you can access the following services:

### Graphite

The graphite dashboard & API is available at: **http://192.168.99.100**
Carbon cache daemon is listening at TCP ports **192.168.99.100:2003** and **192.168.99.100:2004**

### Grafana
This frontend for graphite is available at **http://192.168.99.100/grafana**
Default credentials are configured as: *admin* with password *admin*

### iPython-notebook / jupyter
A complete web python environment is available at   **http://192.168.99.100/ipython**

> **Caveat:** As jupyter was installed in other context that root, due to a nasty bug, web console won't work.



Available directories
------------------

In this repo you'll also find the following interesting directories:

### Volumes

These directories are shared across dockers and your own host.
#### data

Grafana database and whisper files will be created here.

#### notebooks

ipython/jupyter notebooks will be created here.


### Other stuff

#### scripts

miscelaneous scripts to be used will be here.

Tips 'n tricks
------------------

* ```docker-compose kill``` will kill all running dockers
* ```docker-compose up``` will start with all the containers attached to the log (helpful for debugging problems)
* ```docker-compose run [docker-name] bash``` will get you a shell within a docker (great for executing scripts )
* ```docker-compose fetch will``` will update the docker images to the latest version.
