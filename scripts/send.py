#!/usr/bin/env python
import time
import socket
import sys
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Send metric to graphite.')

    parser.add_argument('metric', help='Metric name.')
    parser.add_argument('timestamp', type=int, help='Unix timestamp.')
    parser.add_argument('value', type=float, help='Metric value.')
    parser.add_argument('--port',
        type=int,
        default=2003,
        help='TCP port to send the metric. (default: %(default)s)')
    parser.add_argument('--host',
        default='localhost',
        help='Where to send the metric. (default: %(default)s)')

    args = parser.parse_args()

    return (args.metric, args.value, args.timestamp, args.host, args.port)

def collect_metric(name, value, timestamp, host, port):
    sock = socket.socket()
    sock.connect( (host, port) )
    sock.send("%s %d %d\n" % (name, value, timestamp))
    sock.close()


if __name__ == "__main__":
    collect_metric(*parse_args())
