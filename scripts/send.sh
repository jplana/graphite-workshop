#!/usr/bin/env bash
timestamp=$(date +%s)
host=${3:-localhost}
port=${4:-2003}
echo Sending metric $1 to $host
echo "$1 $2 $timestamp" | nc -c $host $port
